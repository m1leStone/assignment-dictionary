global print_string
global print_string_stderr
global print_char
global print_char_stderr
global print_newline
global print_newline_stderr
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy
global string_length
global exit
global exit_with_error

section .text
  
; Принимает код возврата и завершает текущий процесс
exit: 
    mov    rax, 60         
    syscall

exit_with_error:
    mov     rdi, 1
    mov     rax, 60
    syscall 


; Принимает указатель на нуль-терминированную строку в rdi, возвращает её длину
string_length:
      xor rax, rax            ; обнуляем счетчик
    .count:
      cmp byte [rdi+rax], 0 ; тест на конец строки 
      je .end_str_len
      inc rax               ; инкрементрируем счетчик 
      jmp .count
    .end_str_len:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  call string_length  ;  дилна строки -> rax
  push rsi ; сохраняем callee-saved регистры
  push rdi
  mov     rsi, rdi ; указываем адрес начала строки
  mov     rdx, rax ; указываем длину строки
  mov     rax, 1   ; syscall 'write'
  mov     rdi, 1   ; дискриптор stdout
  syscall
  pop rdi
  pop rsi
  ret

; Принимает код символа в rdi и выводит его в stdout
print_char:
  push rsi  ; сохраняем callee-saved регистры
  push rdi
  mov     rsi, rsp ; все действия аналогичны print_string
  mov     rdx, 1   ; но длина строки всегда 1
  mov     rax, 1
  mov     rdi, 1
  syscall
  pop rdi
  pop rsi
  ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push rdi
    mov rdi, 0xA 
    call print_char
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    
    xor r8, r8              ; счетчик
    mov r10, 10             ; константа, используется для деления (используем calle-saved r10)
    mov rax, rdi            ; заданное число -> rax
    
    .separete_number:
        inc r8            
        xor rdx, rdx        
        div r10             ; деление rax(заданное число) на r11(10) -> целая часть в rax, остаток в rdx 
                            ; остаток - то число, которое необхожимо вывести
        add rdx, 0x30       ; получаем ASCII код символа
        push rdx            ; сохраняем символ в стек
        cmp rax, 0          ; закончилость ли число?
        jz .print_number    ; да -> выводим число
        jmp .separete_number; нет ->считываем следующую цифру
        
    .print_number:
        pop rdi             ; достаем символ из стека
        call print_char     
        dec r8              
        cmp r8, 0           ; всё ли число мы вывели?
        jnz .print_number   ; нет -> выводим следующий символ    

        ret                 ; да -> завершаем программу

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0                  ; число отрицательное?
    js .print_negative_nubmer   ; да -> выводим особым образом

.print_positive_nubmer:         ; нет -> пользуемся print_uint
    call print_uint 
    ret

.print_negative_nubmer:
    push rdi
    mov rdi, 0x2D               ; ASCII код символа "-" 
    call print_char             ; выводим его 
    pop rdi 
    neg rdi 
    call print_uint   ; выводим наше число с противоположным знаком (т.к. "-" уже вывели)
    ret

; Принимает два указателя в rdi и rsi на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        call string_length   ; длины строк совпадают? 
        mov r10, rax         
        push rdi
        mov rdi, rsi
        call string_length
        pop rdi
        cmp rax, r10          
        jnz .return_0        ; нет -> возвращаем 0
        dec r10              ; да  -> сравниваем посимвольно начиная с конца 

    .loop_str_equals:
        cmp r10, 0           ; перебрали все символы и все совпали?
        jl .return_1         ; да -> возвращаем 1
        mov cl, byte [rsi + r10]
        cmp cl, byte [rdi + r10]
        jnz .return_0        ; символы не совпали -> возвращаем 0
        dec r10
        jmp .loop_str_equals

    .return_1:
        mov rax, 1
        ret

    .return_0:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi                ; сохраняем callee-saved регистры
    push rsi
    push 0                  ; выделяем ячейку под читаемый символ

    xor rax, rax            ; 0 - номер системного вызова 'read' 
    xor rdi, rdi            ; дискриптор stdin
    mov rsi, rsp            ; будем класть читаемый символ на вершину стека
    mov rdx, 1              ; длина строки 1 (читаем только 1 символ)
    syscall
    mov rax, [rsp]          ; введенный символ-> rax
    
        pop rdx             ; освобождаем занятую ячейку стека
        pop rsi
        pop rdi
        ret  

; Принимает: адрес начала буфера в rdi, размер буфера в rsi
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    .skip_spaces:
        call read_char          
        cmp rax, 0x09                ; проверка на табуляцию
        je .skip_spaces         
        cmp rax, 0x0A                ; проверка на символ перевода строки
        je .skip_spaces       
        cmp rax, 0x20                ; проверка на пробел
        je .skip_spaces        
        
        xor rdx, rdx                 ; обнуляем счетчик
    .read_word_loop:
        cmp rdx, rsi                 ; счётчик = размеру буфера ?
        je .buffer_overflow_error    ; да -> завершаем подпрограмму
                                     
        cmp rax, 0              ; проверка на нуль-терминатор
        je .end_read_word
        cmp rax, 0x09           ; проверка на табуляцию
        je .end_read_word         
        cmp rax, 0x0A           ; проверка на символ перевода строки
        je .end_read_word       
        cmp rax, 0x20           ; проверка на пробел
        je .end_read_word        

        mov byte [rdi + rdx], al; добавляем в буффер символ
        inc rdx
        push rdx                ; увеличиваем счетчик считанных символов
        call read_char          ; следующий символ -> rax
        pop rdx
        jmp .read_word_loop     ; в начало цикла
    
    .buffer_overflow_error:
        xor rax, rax              ; возвращаем 0 в rax
        ret
    
    .end_read_word:
        mov byte [rdi + rdx], 0 ; добавляем нуль-терминатор
        mov rax, rdi            ; возвращаем адрес начала буффера
        ret
 
 

; Принимает указатель на строку в rdi, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rbx
    xor rbx, rbx            ; буфер для чтения символов
    mov r10, 10             ; константа, используется для деления
    xor rax, rax
    mov rdx, 0              ; счетчик колличества символов

    .loop_parse_uint:         
        mov bl, byte [rdi + rdx]; считываем символ
        cmp bl, 0x30        ;числа в кодировке ASCII имеют коды 0x30 - 0x39     
        jl .end_parse_uint ; поэтому любой другой код будет означать невалидность строки                 
        cmp bl, 0x39            
        jg .end_parse_uint

        sub bl, 0x30 ; заменяем ASCII код числа самим числом
        push rdx
        mul r10      ; умножаем нынешнее число на 10 (e.g. 12 -> 120)
        pop rdx
        add rax, rbx ; прибавляем считанный символ
        inc rdx      ; инкрементируем счетчик
        jmp .loop_parse_uint
        
    .end_parse_uint:
        pop rbx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
        push rdi
    .check_sign:
        mov rax, rdi            
        cmp byte [rax], 0x2D      ; первый символ "-"?
        jz .parse_negative_number ; да -> выводим отрицательное число 
        call parse_uint           ; нет -> используем parse_uint
        jmp .end_parse_int
            
    .parse_negative_number:
        inc rdi                   ; указатель на строку сдвигаем на 1
        call parse_uint           ; используем предыдущую функцию
        cmp rdx, 0                ;  parse_uint вернул 0?
        jz .end_parse_int        ; да -> возвращаем 0
        inc rdx                   ; нет -> добавляем 1 к считанным символам (знак "-")
        neg rax                   ; число представляем как отрицательное
        jmp .end_parse_int 
    
    .end_parse_int:
        pop rdi
        ret

; Принимает указатель на строку в rdi, указатель на буфер в rsi и длину буфера в rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
        call string_length
        cmp rax, rdx
        jg .return_0_str_copy
        xor rcx, rcx
    
    .str_loop:
        cmp rcx, rdx; 
        jge .return_length;  
        mov r11, [rdi + rcx]
        mov [rsi + rcx], r11
        inc rcx; rcx++
        jmp .str_loop
    
    .return_length:
        mov rax, rdx
        ret

    .return_0_str_copy: 
        xor rax, rax
        ret
