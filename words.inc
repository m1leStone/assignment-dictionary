%include 'colon.inc'

colon "garage", garage
db "a building for housing a motor vehicle", 10, 0

colon "robot", robot
db "a machine capable of carrying out a complex series of actions automatically", 10, 0

colon "car", car
db "a four-wheeled road vehicle powered by an engine", 10, 0

colon "key", key
db "a piece of profiled metal with notches intended for opening the lock", 10, 0