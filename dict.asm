extern string_equals
extern string_length
global find_word

section .text

find_word:
	.loop:    
    	push rsi
	    add rsi, 8
	    call string_equals
	    pop rsi
	    cmp rax, 0
	    jnz .end_found

	    mov rsi, [rsi]
	    cmp rsi, 0
	    jz .end_not_found
	    jmp .loop

	.end_not_found:
	    xor rax, rax
	    ret

	.end_found:
	    add rsi, 8
	    ;push rsi
	    call string_length
	    ;pop rsi
	    add rax, rsi
	    inc rax
	    ret