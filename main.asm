%include "words.inc"
%include "dict.inc"
extern print_string, print_string_stderr
extern read_word, exit, exit_with_error

%define MAX_LENGTH 256

global _start

section .data
    welcome_message: db "Enter a key: ", 10, 0
    key_found_message: db "Meaning: ", 0
    key_not_found_message: db "No such key", 10, 0
    error_message: db "Invalid input. Your string must be less than ", MAX_LENGTH, 
    " characters", 10, 0

section .text
_start:
    sub rsp, MAX_LENGTH                ;выделяем место в стеке
    mov rsi, MAX_LENGTH
    mov rdi, rsp
    call read_word              ; читаем ключ
    cmp rdx, MAX_LENGTH
    jg .print_error              ; maybe this is redundatn
    cmp rax, 0
    jz .print_error

    mov rsi, entry ; ищем совпадение
    mov rdi, rax
    call find_word
    cmp rax, 0
    jz .print_key_not_found_error ; если нет совпадений


    mov rdi, rax ; выводим значение
    call print_string
    add rsp, MAX_LENGTH ; освобождаем стэк
    call exit

.print_key_not_found_error:
    mov rdi, key_not_found_message
    call print_string_stderr
    call exit_with_error

.print_error:
    mov rdi, error_message
    call print_string_stderr
    call exit_with_error
  