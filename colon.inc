%define entry 0

%macro colon 2
	%ifid %2
   		%%next: dq entry
	%else
		 %fatal "invalid label"
	%endif

	%ifstr %1
    	db %1, 0
   		%2:
	%else
		 %fatal "key value must be the string"
	%endif
%define entry %%next
%endmacro