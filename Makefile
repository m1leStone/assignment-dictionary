.PHONY: clean 

CFLAGS = -f elf64 -o

main: main.o dict.o lib.o
	ld $^ -o $@

%.o: %.asm
	nasm $(CFLAGS) $@ $^	

clean:
	rm -rf *.o main